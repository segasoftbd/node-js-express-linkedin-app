﻿'use strict';
var express = require('express');
var router = express.Router();

var session = require('express-session');
router.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

// Authentication and Authorization Middleware
var auth = function (req, res, next) {
    if (req.session.userId!=null) {
        console.log('>>>Id:', req.session.userId);
        return next();
    }
    else
        {

            console.log('>>>Id : NULL:', req.session.userId);
            return res.redirect('/login');
        }

};




/* GET home page. */
router.get('/', auth, function (req, res, next) {
    res.render('index', {user: req.session.username});
});

module.exports = router;
