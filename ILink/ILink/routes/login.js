﻿'use strict';
var express = require('express');
var router = express.Router();
var async = require('async');

router.get('/login', function (req, res, next) {
    req.session.userId = null;
    req.session.username = null;
    req.session.userCompanyId = null;
    req.session.userCompanyName = null;
        res.render('login');
});
router.post('/login', function (req, res, next) {
    if (!req.body.Email || !req.body.Password) {
        req.session.userId = null;
        req.session.username = null;
        req.session.userCompanyId = null;
        req.session.userCompanyName = null;

        res.redirect('/');
    } else {

        var email = req.body.Email;
        var pass = req.body.Password;
        var con = req.con;
        async.parallel([
            function (callback) {
                con.query('select * from user WHERE Email="' + email + '" and Password = "' + pass + '"', function (errors, users) {
                    //if (!errors) {
                    //    if (users) {
                    //        callback(null, users);
                    //    }
                    //} else {
                    //    callback(errors, null);
                    //    console.log('Error while performing Query.');
                    //}

                    callback(errors, users);

                });

            }

        ],
            function (err, results) {
                if (results[0] == '')
                {
                    req.session.userId = null;
                    req.session.username = null;
                    req.session.userCompanyId = null;
                    req.session.userCompanyName = null;

                    console.log('un done');
                    res.redirect('/login');
                }
                else  {
                    var data = JSON.stringify(results[0]);
                    var json_data = JSON.parse(data);
                    console.log(json_data[0].Email, email);

                    if (json_data[0].Email == email) {
                        req.session.username = json_data[0].Name;
                        req.session.userId = json_data[0].Id;
                        req.session.userCompanyId = json_data[0].CompanyId;
                        req.session.userCompanyName = json_data[0].CompanyName;

                        console.log('done');
                        res.redirect('/');
                    }
                }

            }
        );

    }
});
router.get('/login/logout', function (req, res, next) {
    req.session.userId = null;
    req.session.username = null;
    req.session.userCompanyId = null;
    req.session.userCompanyName = null;
    res.redirect('/login');
});
module.exports = router;
