﻿
var express = require('express');
var router = express.Router();
var async = require('async');



router.get('/', function (req, res, next) {
    var con = req.con;
    async.parallel([
        function (callback) {

            con.query('select * from company', function (errors, companies) {
                callback(errors, companies);

            });
            
        } 
        
        ],
        function (err, results) {
            var data = { companies: results[0], user: req.session.username };
            res.render('company/index', data);
        }
    );

});

router.get('/add', function (req, res, next) {
    console.log(req.session.userId);
    res.render('company/add', { user: req.session.username });
});

router.post('/add', function (req, res, next) {
    var con = req.con;
    var user_Id = req.session.userId;
    var q = 'select * from company where userid="' + user_Id + '"';
    console.log(q);
    async.parallel([
        function (callback) {


            con.query(q, function (errors, companies) {
                callback(errors, companies);

            });

        }

    ],
        function (err, results) {
            if (results[0] == '') {
                var companyName = req.body.Name;
                async.parallel([
                    function (callback) {

                        con.query('insert into company (Name,Size,Type,Industry,UserId) values (?,?,?,?,?)',
                            [req.body.Name, req.body.Size, req.body.Type, req.body.Industry, req.session.userId], function (errors, company) {
                                callback(errors, company);

                            });

                    }

                ],
                    function (err, results) {
                        
                        var update_user_query = 'UPDATE user  SET CompanyName="' + companyName + '",   CompanyId ="' + results[0].insertId + '"  WHERE Id ="' + user_Id + '"';
                        console.log(results[0].insertId);
                        console.log('Company Name:-------------------------------------------------', companyName);
                        async.parallel([
                            function (callback) {

                                con.query(update_user_query, function (errors, users) {
                                    callback(errors, users);

                                    });

                            }

                        ],
                            function (err, results) {

                            }
                        );
                    }
                );
                
            }
            
        }
    );

    res.redirect('/company');

});



router.get('/SendRequest', function (req, res, next) {
    var con = req.con;
    var user_Id = req.session.userId;
    console.log(user_Id);
    async.parallel([
        function (callback) {

            con.query('select * from company where userId<>"' + user_Id + '"', function (errors, companies) {
                callback(errors, companies);

            });

        }

    ],
        function (err, results) {
            var data = { companies: results[0], user: req.session.username };
            res.render('company/SendRequest', data);
        }
    );

});


router.post('/SendRequest', function (req, res, next) {
    var user_Id = req.session.userId;
    console.log('User:', user_Id);
    var id = req.body.Company_id;
    var companyName = req.session.userCompanyName;
    var mycompanyId = req.session.userCompanyId;
    var username = req.session.username;
    console.log('ID:----', id);
    var con = req.con;
    async.parallel([
        function (callback) {

            con.query('insert into request (CompanyId,RequestFromId,RequestFromName,Status) values (?,?,?,?)',
                [id, mycompanyId, companyName, "REQUESTED"], function (errors, request) {
                    callback(errors, request);

                });

        }

    ],
        function (err, results) {

        }
    );

    res.redirect('/company/SendRequest');

});

router.get('/Request', function (req, res, next) {
    var con = req.con;
    var user_Id = req.session.userId;
    var companyId = req.session.userCompanyId;
    console.log(user_Id);
    var q = 'select * from request where  status="REQUESTED" and companyId="' + companyId + '"';
    console.log(q);
    async.parallel([
        function (callback) {

            con.query(q, function (errors, request) {
                callback(errors, request);

            });

        }

    ],
        function (err, results) {
            console.log('RESULT:',results[0])
            var data = { companies: results[0], user: req.session.username};
            res.render('company/request', data);
        }
    );

});



router.post('/Accept', function (req, res, next) {
    var con = req.con;
    var myuser_Id = req.session.userId;
    var mycompanyId = req.session.userCompanyId;
    var requestFromId = req.body.companyId;
    var requestFromName = req.body.RequestFromName;
    var reqId = req.body.Id;

    var q = 'UPDATE user  SET CompanyName="' + requestFromName + '",   CompanyId ="' + requestFromId + '"  WHERE CompanyId ="' + mycompanyId + '"';
    console.log(q);
    async.parallel([
        function (callback) {

            con.query(q, function (errors, request) {
                callback(errors, request);

            });

        }

    ],
        function (err, results) {
            var update_Q = 'UPDATE request  SET Status="ACCEPTED"  WHERE Id ="' + reqId + '"';
            console.log(update_Q);
            async.parallel([
                function (callback) {

                    con.query(update_Q, function (errors, request) {
                        callback(errors, request);

                    });

                }

            ],
                function (err, results) {

                }
            );
        }
    );
    res.redirect('/company/request');
});




router.post('/Reject', function (req, res, next) {
    var con = req.con;
    var myuser_Id = req.session.userId;
    var mycompanyId = req.session.userCompanyId;
    var requestFromId = req.body.companyId;
    var requestFromName = req.body.RequestFromName;
    var reqId = req.body.Id;

    var q = 'UPDATE request  SET Status="REJECTED"  WHERE Id ="' + reqId + '"';
    console.log(q);
    async.parallel([
        function (callback) {

            con.query(q, function (errors, request) {
                callback(errors, request);

            });

        }

    ],
        function (err, results) {

        }
    );
    res.redirect('/company/request');
});


router.post('/addmycompany', function (req, res, next) {
    var con = req.con;
    var myuser_Id = req.session.userId;
    var mycompanyId = req.session.userCompanyId;
    var mycompanyName = req.session.userCompanyName;
    var Id = req.body.Id;
    var q = 'UPDATE user  SET CompanyName="' + mycompanyName + '",   CompanyId ="' + mycompanyId + '"  WHERE Id ="' + Id + '"';
    console.log(q);
    async.parallel([
        function (callback) {

            con.query(q, function (errors, request) {
                callback(errors, request);

            });

        }

    ],
        function (err, results) {

        }
    );
    res.redirect('/user/');
});





module.exports = router;
