﻿
var express = require('express');
var router = express.Router();
var async = require('async');



router.get('/', function (req, res, next) {
    var con = req.con;
    async.parallel([
        function (callback) {

            con.query('select * from user', function (errors, users) {
                callback(errors, users);

            });

        }

    ],
        function (err, results) {
            var data = { users: results[0], user: req.session.username };
            res.render('user/index', data);
        }
    );

});
router.post('/', function (req, res, next) {
    console.log('POST');
    var con = req.con;
    var email = req.body.Email;
    async.parallel([
        function (callback) {

            con.query('select * from user WHERE Email="' + email + '"', function (errors, users) {
                callback(errors, users);

            });

        }

    ],
        function (err, results) {
            var data = { users: results[0], user: req.session.username };
            res.render('user/index', data);

        }
    );

});

router.get('/signup', function (req, res, next) {
    res.render('user/signup');
});

router.post('/signup', function (req, res, next) {
    var con = req.con;
    async.parallel([
        function (callback) {

            con.query('insert into user (Name,Email,Password) values (?,?,?)',
                [req.body.Name, req.body.Email, req.body.Password], function (errors, users) {
                    callback(errors);

                });

        }

    ],
        function (err, results) {
            res.redirect('/');
        }
    );
});

router.get('/Connected', function (req, res, next) {
    var user_Id = req.session.userId;
    console.log('User:', user_Id);
    var id = req.body.Company_id;
    var companyName = req.session.userCompanyName;
    var mycompanyId = req.session.userCompanyId;
    var con = req.con;
    async.parallel([
        function (callback) {

            con.query('select * from user where companyId="' + mycompanyId + '"', function (errors, users) {
                callback(errors, users);

            });

        }

    ],
        function (err, results) {
            var data = { users: results[0], user: req.session.username  };
            res.render('user/Connected', data);
        }
    );

});

module.exports = router;
